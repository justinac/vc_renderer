#ifndef SURFACE_H
#define SURFACE_H

#include "vc_def.h"

VkResult vc_surface_create(vc_renderer_t *pRenderer);
void vc_surface_destroy(vc_renderer_t *pRenderer);

#endif // SURFACE_H