#include "vc_device.h"

VkResult vc_device_create(vc_renderer_t *pRenderer) {    
    unsigned int queueFamilyCount = 0;

    {
        vkGetPhysicalDeviceQueueFamilyProperties(pRenderer->gpu.physicalDevice, &queueFamilyCount, NULL);
        VkQueueFamilyProperties *queueFamilyProperties = (VkQueueFamilyProperties*)malloc(sizeof(VkQueueFamilyProperties) * queueFamilyCount);
        vkGetPhysicalDeviceQueueFamilyProperties(pRenderer->gpu.physicalDevice, &queueFamilyCount, queueFamilyProperties);

        bool foundQueueFamily = false;

        for (int i = 0; i < queueFamilyCount; i++) {
            if (queueFamilyProperties[i].queueFlags & VK_QUEUE_GRAPHICS_BIT) {
                pRenderer->queueFamilyIndex = i;
                foundQueueFamily = true;
                break;
            }
        }

        free(queueFamilyProperties);
    }

    const char *deviceExtensions[] = {
        VK_KHR_SWAPCHAIN_EXTENSION_NAME
    };

    float queuePriorities[] = { 1.0f };
    VkDeviceQueueCreateInfo deviceQueueCreateInfo = {};
    deviceQueueCreateInfo.sType             = VK_STRUCTURE_TYPE_DEVICE_QUEUE_CREATE_INFO;
    deviceQueueCreateInfo.queueFamilyIndex  = pRenderer->queueFamilyIndex;
    deviceQueueCreateInfo.queueCount        = 1;
    deviceQueueCreateInfo.pQueuePriorities  = queuePriorities;

    VkDeviceCreateInfo deviceCreateInfo = {};
    deviceCreateInfo.sType                      = VK_STRUCTURE_TYPE_DEVICE_CREATE_INFO;
    deviceCreateInfo.queueCreateInfoCount       = 1;
    deviceCreateInfo.pQueueCreateInfos          = &deviceQueueCreateInfo;
    deviceCreateInfo.enabledLayerCount          = 0;
    deviceCreateInfo.ppEnabledLayerNames        = NULL;
    deviceCreateInfo.enabledExtensionCount      = 1;
    deviceCreateInfo.ppEnabledExtensionNames    = deviceExtensions;
    deviceCreateInfo.pEnabledFeatures           = NULL;

    return (vkCreateDevice(pRenderer->gpu.physicalDevice, &deviceCreateInfo, NULL, &pRenderer->logicalDevice));
}

void vc_device_destroy(vc_renderer_t *pRenderer) {
    vkDestroyDevice(pRenderer->logicalDevice, VK_NULL_HANDLE);
}
