#include "vc_buffer.h"

ubo_t ubo;

float moveSpeed;
float mouseSpeed;

vec3_t position;
vec3_t forward;
vec3_t up;

float yaw   = 0.0f;
float pitch = 0.0f;

void vc_buffer_copy(vc_renderer_t *pRenderer, VkBuffer dest, VkBuffer src, VkDeviceSize size) {
    VkCommandBufferAllocateInfo allocInfo = {};
    allocInfo.sType                 = VK_STRUCTURE_TYPE_COMMAND_BUFFER_ALLOCATE_INFO;
    allocInfo.commandPool           = pRenderer->commandPool;
    allocInfo.level                 = VK_COMMAND_BUFFER_LEVEL_PRIMARY;
    allocInfo.commandBufferCount    = 1;

    VkCommandBuffer commandBuffer;
    vkAllocateCommandBuffers(pRenderer->logicalDevice, &allocInfo, &commandBuffer);

    VkCommandBufferBeginInfo beginInfo = {};
    beginInfo.sType             = VK_STRUCTURE_TYPE_COMMAND_BUFFER_BEGIN_INFO;
    beginInfo.flags             = VK_COMMAND_BUFFER_USAGE_ONE_TIME_SUBMIT_BIT;

    vkBeginCommandBuffer(commandBuffer, &beginInfo);

    VkBufferCopy copyRegion = {};
    copyRegion.size = size;
    vkCmdCopyBuffer(commandBuffer, src, dest, 1, &copyRegion);

    vkEndCommandBuffer(commandBuffer);

    VkSubmitInfo submitInfo = {};
    submitInfo.sType                = VK_STRUCTURE_TYPE_SUBMIT_INFO;
    submitInfo.commandBufferCount   = 1;
    submitInfo.pCommandBuffers      = &commandBuffer;

    vkQueueSubmit(pRenderer->graphicsQueue, 1, &submitInfo, VK_NULL_HANDLE);
    vkQueueWaitIdle(pRenderer->graphicsQueue);

    vkFreeCommandBuffers(pRenderer->logicalDevice, pRenderer->commandPool, 1, &commandBuffer);
}

void vc_buffer_create(vc_renderer_t *pRenderer, VkDeviceSize size, VkBufferUsageFlags usage, VkMemoryPropertyFlags properties, VkBuffer *pBuffer, VkDeviceMemory *pBufferMemory) {
    VkBufferCreateInfo bufferInfo = {};
    bufferInfo.sType                    = VK_STRUCTURE_TYPE_BUFFER_CREATE_INFO;
    bufferInfo.size                     = size;
    bufferInfo.usage                    = usage;
    bufferInfo.sharingMode              = VK_SHARING_MODE_EXCLUSIVE;
    bufferInfo.queueFamilyIndexCount    = 0;
    bufferInfo.pQueueFamilyIndices      = NULL;

    VC_CHECK(vkCreateBuffer(pRenderer->logicalDevice, &bufferInfo, NULL, pBuffer));

    VkMemoryRequirements memRequirements;
    vkGetBufferMemoryRequirements(pRenderer->logicalDevice, *pBuffer, &memRequirements);

    VkMemoryAllocateInfo allocInfo = {};
    allocInfo.sType             = VK_STRUCTURE_TYPE_MEMORY_ALLOCATE_INFO;
    allocInfo.allocationSize    = memRequirements.size;
    allocInfo.memoryTypeIndex   = gpu_get_memory_type(pRenderer, memRequirements.memoryTypeBits, properties);

    VC_CHECK(vkAllocateMemory(pRenderer->logicalDevice, &allocInfo, NULL, pBufferMemory));

    vkBindBufferMemory(pRenderer->logicalDevice, *pBuffer, *pBufferMemory, 0);
}

void vc_buffer_vertex_create(vc_renderer_t *pRenderer, unsigned int verticesSize, vertex_t *vertices) {
    VkDeviceSize bufferSize = verticesSize;

    VkBuffer stagingBuffer;
    VkDeviceMemory stagingBufferMemory;

    vc_buffer_create(pRenderer, bufferSize, VK_BUFFER_USAGE_TRANSFER_SRC_BIT, VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT | VK_MEMORY_PROPERTY_HOST_COHERENT_BIT, &stagingBuffer, &stagingBufferMemory);

    void *data;
    vkMapMemory(pRenderer->logicalDevice, stagingBufferMemory, 0, bufferSize, 0, &data);
    memcpy(data, vertices, bufferSize);
    vkUnmapMemory(pRenderer->logicalDevice, stagingBufferMemory);

    vc_buffer_create(pRenderer, bufferSize, VK_BUFFER_USAGE_TRANSFER_DST_BIT | VK_BUFFER_USAGE_VERTEX_BUFFER_BIT, VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT, &pRenderer->vertexBuffer, &pRenderer->vertexBufferMemory);

    vc_buffer_copy(pRenderer, pRenderer->vertexBuffer, stagingBuffer, bufferSize);

    vkDestroyBuffer(pRenderer->logicalDevice, stagingBuffer, VK_NULL_HANDLE);
    vkFreeMemory(pRenderer->logicalDevice, stagingBufferMemory, VK_NULL_HANDLE);
}

void vc_buffer_vertex_destroy(vc_renderer_t *pRenderer) {
    vkDestroyBuffer(pRenderer->logicalDevice, pRenderer->vertexBuffer, VK_NULL_HANDLE);
    vkFreeMemory(pRenderer->logicalDevice, pRenderer->vertexBufferMemory, VK_NULL_HANDLE);
}

void vc_buffer_index_create(vc_renderer_t *pRenderer, unsigned int indicesSize, unsigned short *indices) {
    VkDeviceSize bufferSize = indicesSize;

    VkBuffer stagingBuffer;
    VkDeviceMemory stagingBufferMemory;
    vc_buffer_create(pRenderer, bufferSize, VK_BUFFER_USAGE_TRANSFER_SRC_BIT, VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT | VK_MEMORY_PROPERTY_HOST_COHERENT_BIT, &stagingBuffer, &stagingBufferMemory);

    void *data;
    vkMapMemory(pRenderer->logicalDevice, stagingBufferMemory, 0, bufferSize, 0, &data);
    memcpy(data, indices, bufferSize);
    vkUnmapMemory(pRenderer->logicalDevice, stagingBufferMemory);

    vc_buffer_create(pRenderer, bufferSize, VK_BUFFER_USAGE_TRANSFER_DST_BIT | VK_BUFFER_USAGE_INDEX_BUFFER_BIT, VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT, &pRenderer->indexBuffer, &pRenderer->indexBufferMemory);

    vc_buffer_copy(pRenderer, pRenderer->indexBuffer, stagingBuffer, bufferSize);

    vkDestroyBuffer(pRenderer->logicalDevice, stagingBuffer, VK_NULL_HANDLE);
    vkFreeMemory(pRenderer->logicalDevice, stagingBufferMemory, VK_NULL_HANDLE);
}

void vc_buffer_index_destroy(vc_renderer_t *pRenderer) {
    vkDestroyBuffer(pRenderer->logicalDevice, pRenderer->indexBuffer, VK_NULL_HANDLE);
    vkFreeMemory(pRenderer->logicalDevice, pRenderer->indexBufferMemory, VK_NULL_HANDLE);
}

void vc_buffer_uniform_create(vc_renderer_t *pRenderer) {
    pRenderer->pUniformBuffers          = malloc(sizeof(VkBuffer) * pRenderer->imageCount);
    pRenderer->pUniformBuffersMemory    = malloc(sizeof(VkDeviceMemory) * pRenderer->imageCount);

    VkDeviceSize bufferSize = sizeof(ubo_t);

    moveSpeed   = 0.25f;
    mouseSpeed  = 0.05f;

    // Model.
    mat4_identity(&ubo.model);
    mat4_translate(&ubo.model, (vec3_t){ 0, 0, -10 });

    // View.
    position    = (vec3_t){ 0, 0, 3 };
    forward     = (vec3_t){ 0, 0,-1 };
    up          = (vec3_t){ 0,-1, 0 };

    mat4_identity(&ubo.view);
    mat4_look_at(&ubo.view, position, forward, up);

    // Projection / Perspective.
    mat4_projection(&ubo.proj, deg_to_rad(45), 1.5, 0, 100);

    for (int i = 0; i < pRenderer->imageCount; i++) {
        vc_buffer_create(pRenderer, bufferSize, VK_BUFFER_USAGE_UNIFORM_BUFFER_BIT, VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT | VK_MEMORY_PROPERTY_HOST_COHERENT_BIT, &pRenderer->pUniformBuffers[i], &pRenderer->pUniformBuffersMemory[i]);
    }
}

void vc_buffer_uniform_destroy(vc_renderer_t *pRenderer) {
    for (int i = 0; i < pRenderer->imageCount; i++) {
        vkDestroyBuffer(pRenderer->logicalDevice, pRenderer->pUniformBuffers[i], VK_NULL_HANDLE);
        vkFreeMemory(pRenderer->logicalDevice, pRenderer->pUniformBuffersMemory[i], VK_NULL_HANDLE);
    }

    free(pRenderer->pUniformBuffers);
    free(pRenderer->pUniformBuffersMemory);
}

// NOTE(@justinac): Later on all matrix manipulation will be done through an Entity struct / funcs.
void vc_buffer_uniform_update(vc_renderer_t *pRenderer, unsigned int imageIndex) {
    // Camera manipulation code.
    // Forward/Back.
    if (glfwGetKey(pRenderer->pWindow, GLFW_KEY_W) == GLFW_PRESS) {
        position = vec3_add(position, vec3_mul_scalar(forward, moveSpeed));
    }
    if (glfwGetKey(pRenderer->pWindow, GLFW_KEY_S) == GLFW_PRESS) {
        position = vec3_sub(position, vec3_mul_scalar(forward, moveSpeed));
    }

    // Left/Right.
    if (glfwGetKey(pRenderer->pWindow, GLFW_KEY_D) == GLFW_PRESS) {
        position = vec3_add(position, vec3_mul_scalar(vec3_normalize(vec3_cross(forward, up)), moveSpeed));
    }
    if (glfwGetKey(pRenderer->pWindow, GLFW_KEY_A) == GLFW_PRESS) {
        position = vec3_sub(position, vec3_mul_scalar(vec3_normalize(vec3_cross(forward, up)), moveSpeed));
    }

    // Up/Down.
    if (glfwGetKey(pRenderer->pWindow, GLFW_KEY_SPACE) == GLFW_PRESS) {
        position.y += moveSpeed;
    }
    if (glfwGetKey(pRenderer->pWindow, GLFW_KEY_LEFT_SHIFT) == GLFW_PRESS) {
        position.y -= moveSpeed;
    }

    float lastX = window_get_size(pRenderer->pWindow).width  / 2.0f;
    float lastY = window_get_size(pRenderer->pWindow).height / 2.0f;

    double xpos, ypos;
    glfwGetCursorPos(pRenderer->pWindow, &xpos, &ypos);
    glfwSetCursorPos(pRenderer->pWindow, lastX, lastY);

    float xOffs = xpos - lastX;
    float yOffs = lastY - ypos;
    lastX       = xpos;
    lastY       = ypos;

    xOffs *= mouseSpeed;
    yOffs *= mouseSpeed;

    yaw   -= xOffs;
    pitch += yOffs;

    if (pitch > 89.9f) {
        pitch = 89.9f;
    }
    if (pitch < -89.9f) {
        pitch = -89.9f;
    }

    forward = vec3_normalize((vec3_t) {
        cos(deg_to_rad(yaw)) * cos(deg_to_rad(pitch)),
        sin(deg_to_rad(pitch)),
        sin(deg_to_rad(yaw)) * cos(deg_to_rad(pitch))
    });

    mat4_look_at(&ubo.view, position, vec3_add(position, forward), up);

    void *data;
    vkMapMemory(pRenderer->logicalDevice, pRenderer->pUniformBuffersMemory[imageIndex], 0, sizeof(ubo), 0, &data);
    memcpy(data, &ubo, sizeof(ubo));
    vkUnmapMemory(pRenderer->logicalDevice, pRenderer->pUniformBuffersMemory[imageIndex]);
}
