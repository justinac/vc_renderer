#include "vc_swapchain.h"

#include "vc_renderpass.h"
#include "vc_graphicspipeline.h"
#include "vc_commandbuffer.h"

VkResult vc_swapchain_create(vc_renderer_t *pRenderer) {
    VkPresentModeKHR presentMode = VK_PRESENT_MODE_FIFO_KHR;

    {
        unsigned int presentModeCount = 0;
        vkGetPhysicalDeviceSurfacePresentModesKHR(pRenderer->gpu.physicalDevice, pRenderer->surface, &presentModeCount, NULL);
        VkPresentModeKHR *presentModes = (VkPresentModeKHR*)malloc(sizeof(VkPresentModeKHR) * presentModeCount);
        vkGetPhysicalDeviceSurfacePresentModesKHR(pRenderer->gpu.physicalDevice, pRenderer->surface, &presentModeCount, presentModes);

        for (int i = 0; i < presentModeCount; i++) {
            if (presentModes[i] == VK_PRESENT_MODE_FIFO_KHR) {
                presentMode = presentModes[i];
            }
        }
        free(presentModes);

        VkSurfaceCapabilitiesKHR surfaceCapabilities;
        vkGetPhysicalDeviceSurfaceCapabilitiesKHR(pRenderer->gpu.physicalDevice, pRenderer->surface, &surfaceCapabilities);

        unsigned int surfaceFormatCount = 0;
        vkGetPhysicalDeviceSurfaceFormatsKHR(pRenderer->gpu.physicalDevice, pRenderer->surface, &surfaceFormatCount, NULL);
        VkSurfaceFormatKHR *surfaceFormats = (VkSurfaceFormatKHR*)malloc(sizeof(VkSurfaceFormatKHR) * surfaceFormatCount);
        vkGetPhysicalDeviceSurfaceFormatsKHR(pRenderer->gpu.physicalDevice, pRenderer->surface, &surfaceFormatCount, surfaceFormats);        

        free(surfaceFormats);

        VkBool32 supported;
        vkGetPhysicalDeviceSurfaceSupportKHR(pRenderer->gpu.physicalDevice, pRenderer->queueFamilyIndex, pRenderer->surface, &supported);
    }

    VkSwapchainCreateInfoKHR swapchainCreateInfo = {};
    swapchainCreateInfo.sType                   = VK_STRUCTURE_TYPE_SWAPCHAIN_CREATE_INFO_KHR;
    swapchainCreateInfo.surface                 = pRenderer->surface;
    swapchainCreateInfo.minImageCount           = 2;
    swapchainCreateInfo.imageFormat             = VC_DEFAULT_IMAGE_FORMAT;
    swapchainCreateInfo.imageColorSpace         = VK_COLOR_SPACE_SRGB_NONLINEAR_KHR;
    swapchainCreateInfo.imageExtent             = window_get_size(pRenderer->pWindow);
    swapchainCreateInfo.imageArrayLayers        = 1;
    swapchainCreateInfo.imageUsage              = VK_IMAGE_USAGE_COLOR_ATTACHMENT_BIT;
    swapchainCreateInfo.imageSharingMode        = VK_SHARING_MODE_EXCLUSIVE;
    swapchainCreateInfo.queueFamilyIndexCount   = 0;
    swapchainCreateInfo.pQueueFamilyIndices     = NULL;
    swapchainCreateInfo.preTransform            = VK_SURFACE_TRANSFORM_IDENTITY_BIT_KHR;
    swapchainCreateInfo.compositeAlpha          = VK_COMPOSITE_ALPHA_OPAQUE_BIT_KHR;
    swapchainCreateInfo.presentMode             = presentMode;
    swapchainCreateInfo.clipped                 = VK_TRUE;
    swapchainCreateInfo.oldSwapchain            = VK_NULL_HANDLE;

    return (vkCreateSwapchainKHR(pRenderer->logicalDevice, &swapchainCreateInfo, NULL, &pRenderer->swapchain));
}

void vc_swapchain_destroy(vc_renderer_t *pRenderer) {
    vkDestroySwapchainKHR(pRenderer->logicalDevice, pRenderer->swapchain, VK_NULL_HANDLE);
}

void vc_swapchain_recreate(vc_renderer_t *pRenderer, unsigned int indicesCount) {
    vkDeviceWaitIdle(pRenderer->logicalDevice);

    vc_commandbuffers_destroy(pRenderer);
    vc_swapchain_framebuffers_destroy(pRenderer);
    vc_graphicspipeline_destroy(pRenderer);
    vc_renderpass_destroy(pRenderer);
    vc_swapchain_images_destroy(pRenderer);
    vc_swapchain_destroy(pRenderer);

    vc_swapchain_create(pRenderer);
    vc_swapchain_images_create(pRenderer);
    vc_renderpass_create(pRenderer);
    vc_graphicspipeline_create(pRenderer);
    vc_swapchain_framebuffers_create(pRenderer);
    vc_commandbuffers_create(pRenderer, indicesCount);
}

void vc_swapchain_images_create(vc_renderer_t *pRenderer) {
    vkGetSwapchainImagesKHR(pRenderer->logicalDevice, pRenderer->swapchain, &pRenderer->imageCount, NULL);

    pRenderer->pImages      = (VkImage*)malloc(sizeof(VkImage) * pRenderer->imageCount);
    pRenderer->pImageViews  = (VkImageView*)malloc(sizeof(VkImageView) * pRenderer->imageCount);

    vkGetSwapchainImagesKHR(pRenderer->logicalDevice, pRenderer->swapchain, &pRenderer->imageCount, pRenderer->pImages);

    for (int i = 0; i < pRenderer->imageCount; i++) {
        VkImageViewCreateInfo imageViewInfo = {};
        imageViewInfo.sType             = VK_STRUCTURE_TYPE_IMAGE_VIEW_CREATE_INFO;
        imageViewInfo.image             = pRenderer->pImages[i];
        imageViewInfo.viewType          = VK_IMAGE_VIEW_TYPE_2D;
        imageViewInfo.format            = VK_FORMAT_B8G8R8A8_UNORM;
        imageViewInfo.components        = (VkComponentMapping){ VK_COMPONENT_SWIZZLE_IDENTITY };

        VkImageSubresourceRange imageSubresourceRange = {};
        imageSubresourceRange.aspectMask     = VK_IMAGE_ASPECT_COLOR_BIT;
        imageSubresourceRange.baseMipLevel   = 0;
        imageSubresourceRange.levelCount     = 1;
        imageSubresourceRange.baseArrayLayer = 0;
        imageSubresourceRange.layerCount     = 1;

        imageViewInfo.subresourceRange = imageSubresourceRange;

        VC_CHECK(vkCreateImageView(pRenderer->logicalDevice, &imageViewInfo, NULL, &pRenderer->pImageViews[i]));
        printf("Created VkImageView[%d] - (%p)\n", i, pRenderer->pImageViews[i]);
    }
}

void vc_swapchain_images_destroy(vc_renderer_t *pRenderer) {
    for (int i = 0; i < pRenderer->imageCount; i++) {
        vkDestroyImageView(pRenderer->logicalDevice, pRenderer->pImageViews[i], VK_NULL_HANDLE);
    }

    free(pRenderer->pImages);
    free(pRenderer->pImageViews);
}

void vc_swapchain_framebuffers_create(vc_renderer_t *pRenderer) {
    pRenderer->pFrameBuffers = malloc(sizeof(VkFramebuffer) * pRenderer->imageCount);

    for (int i = 0; i < pRenderer->imageCount; i++) {
        VkImageView attachments[] = {
            pRenderer->pImageViews[i]
        };

        VkFramebufferCreateInfo framebufferInfo = {};
        framebufferInfo.sType             = VK_STRUCTURE_TYPE_FRAMEBUFFER_CREATE_INFO;
        framebufferInfo.renderPass        = pRenderer->renderpass;
        framebufferInfo.attachmentCount   = 1;
        framebufferInfo.pAttachments      = attachments;
        framebufferInfo.width             = window_get_size(pRenderer->pWindow).width;
        framebufferInfo.height            = window_get_size(pRenderer->pWindow).height;
        framebufferInfo.layers            = 1;

        VC_CHECK(vkCreateFramebuffer(pRenderer->logicalDevice, &framebufferInfo, NULL, &pRenderer->pFrameBuffers[i]));
    }
}

void vc_swapchain_framebuffers_destroy(vc_renderer_t *pRenderer) {
    for (int i = 0; i < pRenderer->imageCount; i++) {
        vkDestroyFramebuffer(pRenderer->logicalDevice, pRenderer->pFrameBuffers[i], VK_NULL_HANDLE);
    }

    free(pRenderer->pFrameBuffers);
}
