// Author: @justinac.
// Description: VkInstance manipulation functions.
// (VkInstance is the connection between our application and the Vulkan library).
// License: MIT.

#ifndef VC_INSTANCE_H
#define VC_INSTANCE_H

#include "vc_def.h"

// Create a VkInstance.
VkResult vc_instance_create(VkInstance *pInstance, const char *pApplicationName);

// Destroy a VkInstance.
void vc_instance_destroy(VkInstance *pInstance);

#endif // VC_INSTANCE_H
