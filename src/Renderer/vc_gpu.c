#include "vc_gpu.h"

void vc_gpu_list(vc_renderer_t *pRenderer) {
    unsigned int physicalDeviceCount = 0;

    vkEnumeratePhysicalDevices(pRenderer->instance, &physicalDeviceCount, NULL);
    VkPhysicalDevice *physicalDevices = (VkPhysicalDevice*)malloc(sizeof(VkPhysicalDevice) * physicalDeviceCount);
    vkEnumeratePhysicalDevices(pRenderer->instance, &physicalDeviceCount, physicalDevices);

    pRenderer->gpu.physicalDevice = physicalDevices[0];
    free(physicalDevices);

    vkGetPhysicalDeviceProperties(pRenderer->gpu.physicalDevice, &pRenderer->gpu.properties);
    vkGetPhysicalDeviceFeatures(pRenderer->gpu.physicalDevice, &pRenderer->gpu.features);
}

unsigned int gpu_get_memory_type(vc_renderer_t *pRenderer, unsigned int typeFilter, VkMemoryPropertyFlags properties) {
    VkPhysicalDeviceMemoryProperties memProperties;
    vkGetPhysicalDeviceMemoryProperties(pRenderer->gpu.physicalDevice, &memProperties);

    for (int i = 0; i < memProperties.memoryTypeCount; i++) {
        if (typeFilter & (1 << i) && (memProperties.memoryTypes[i].propertyFlags & properties) == properties) {
            return i;
        }
    }

    printf("Failed to find suitable memory type!\n");
    exit(EXIT_FAILURE);
}
