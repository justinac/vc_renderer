#include "vc_renderer.h"

// Private deps.
#include "vc_instance.h"
#include "vc_gpu.h"
#include "vc_device.h"
#include "vc_surface.h"
#include "vc_swapchain.h"
#include "vc_renderpass.h"
#include "vc_descriptor.h"
#include "vc_graphicspipeline.h"
#include "vc_commandbuffer.h"
#include "vc_buffer.h"
#include "vc_semaphore.h"

vertex_t vertices[24] = {
    {{ -1.0f,  1.0f, -1.0f }, { 1.0f, 1.0f, 1.0f }},
    {{ -1.0f,  1.0f,  1.0f }, { 0.0f, 1.0f, 1.0f }},
    {{  1.0f,  1.0f,  1.0f }, { 0.0f, 0.0f, 1.0f }},
    {{  1.0f,  1.0f, -1.0f }, { 0.0f, 1.0f, 0.0f }},
    {{ -1.0f,  1.0f,  1.0f }, { 1.0f, 1.0f, 0.0f }},
    {{ -1.0f, -1.0f,  1.0f }, { 1.0f, 0.0f, 0.0f }},
    {{ -1.0f, -1.0f, -1.0f }, { 0.0f, 1.0f, 1.0f }},
    {{ -1.0f,  1.0f, -1.0f }, { 0.0f, 0.0f, 1.0f }},
    {{  1.0f,  1.0f,  1.0f }, { 0.0f, 1.0f, 0.0f }},
    {{  1.0f, -1.0f,  1.0f }, { 0.0f, 1.0f, 0.0f }},
    {{  1.0f, -1.0f, -1.0f }, { 1.0f, 0.0f, 0.0f }},
    {{  1.0f,  1.0f, -1.0f }, { 1.0f, 0.0f, 1.0f }},
    {{  1.0f,  1.0f,  1.0f }, { 1.0f, 1.0f, 1.0f }},
    {{  1.0f, -1.0f,  1.0f }, { 0.0f, 1.0f, 0.0f }},
    {{ -1.0f, -1.0f,  1.0f }, { 0.0f, 0.0f, 0.0f }},
    {{ -1.0f,  1.0f,  1.0f }, { 0.0f, 1.0f, 0.0f }},
    {{  1.0f,  1.0f, -1.0f }, { 0.0f, 1.0f, 1.0f }},
    {{  1.0f, -1.0f, -1.0f }, { 1.0f, 0.0f, 1.0f }},
    {{ -1.0f, -1.0f, -1.0f }, { 1.0f, 0.0f, 0.0f }},
    {{ -1.0f,  1.0f, -1.0f }, { 0.0f, 0.0f, 0.0f }},
    {{ -1.0f, -1.0f, -1.0f }, { 1.0f, 1.0f, 0.0f }},
    {{ -1.0f, -1.0f,  1.0f }, { 1.0f, 1.0f, 1.0f }},
    {{  1.0f, -1.0f,  1.0f }, { 1.0f, 0.0f, 1.0f }},
    {{  1.0f, -1.0f, -1.0f }, { 1.0f, 0.0f, 0.0f }}
};

unsigned short indices[36] = {
    0, 1, 2,
    0, 2, 3,
    5, 4, 6,
    6, 4, 7,
    8, 9, 10,
    8, 10, 11,
    13, 12, 14,
    15, 14, 12,
    16, 17, 18,
    16, 18, 19,
    21, 20, 22,
    22, 20, 23
};

vc_renderer_t vc_create(const char *pApplicationName, GLFWwindow* pWindow) {
    vc_renderer_t renderer = {};
    { // Initialize vulkan context renderer.
        renderer.pApplicationName           = pApplicationName;
        renderer.pWindow                    = pWindow;
        renderer.instance                   = VK_NULL_HANDLE;
        renderer.gpu                        = (vc_gpu_t){};
        renderer.logicalDevice              = VK_NULL_HANDLE;
        renderer.queueFamilyIndex           = 0;
        renderer.graphicsQueue              = VK_NULL_HANDLE;
        renderer.presentQueue               = VK_NULL_HANDLE;
        renderer.surface                    = VK_NULL_HANDLE;
        renderer.swapchain                  = VK_NULL_HANDLE;
        renderer.format                     = VK_FORMAT_B8G8R8A8_UNORM;
        renderer.size                       = window_get_size(pWindow);
        renderer.imageCount                 = 0;
        renderer.pImages                    = VK_NULL_HANDLE;
        renderer.pImageViews                = VK_NULL_HANDLE;
        renderer.pFrameBuffers              = VK_NULL_HANDLE;
        renderer.renderpass                 = VK_NULL_HANDLE;
        renderer.pipelineLayout             = VK_NULL_HANDLE;
        renderer.graphicsPipeline           = VK_NULL_HANDLE;
        renderer.commandPool                = VK_NULL_HANDLE;
        renderer.pCommandBuffers            = VK_NULL_HANDLE;
        renderer.pImageAvalibleSemaphores   = VK_NULL_HANDLE;
        renderer.pRenderFinishedSemaphores  = VK_NULL_HANDLE;
        renderer.pInFlightFences            = VK_NULL_HANDLE;
        renderer.vertexBuffer               = VK_NULL_HANDLE;
        renderer.vertexBufferMemory         = VK_NULL_HANDLE;
        renderer.indexBuffer                = VK_NULL_HANDLE;
        renderer.indexBufferMemory          = VK_NULL_HANDLE;
        renderer.pUniformBuffers            = VK_NULL_HANDLE;
        renderer.pUniformBuffersMemory      = VK_NULL_HANDLE;
        renderer.imageIndex                 = 0;
        renderer.descriptorSetLayout        = VK_NULL_HANDLE;
        renderer.descriptorPool             = VK_NULL_HANDLE;
        renderer.pDescriptorSets            = VK_NULL_HANDLE;
    }

    // Create VkInstance handle.
    VC_CHECK(vc_instance_create(&renderer.instance, renderer.pApplicationName));
    printf("Created VkInstance - (%p)\n", renderer.instance);

    // Select a plugged in graphics card.
    vc_gpu_list(&renderer);
    printf("GPU SELECTED: %s - (%p)\n", renderer.gpu.properties.deviceName, renderer.gpu.physicalDevice);

    // Create VkDevice handle.
    VC_CHECK(vc_device_create(&renderer));
    printf("Created VkDevice - (%p)\n", renderer.logicalDevice);

    { // Get graphics and present queue.
        vkGetDeviceQueue(renderer.logicalDevice, renderer.queueFamilyIndex, 0, &renderer.graphicsQueue);
        vkGetDeviceQueue(renderer.logicalDevice, renderer.queueFamilyIndex, 0, &renderer.presentQueue);
        printf("Selected graphicsQueue - (%p)\n", renderer.graphicsQueue);
        printf("Selected presentQueue - (%p)\n", renderer.presentQueue);
    }

    // Create VkSurfaceKHR handle.
    VC_CHECK(vc_surface_create(&renderer));
    printf("Created VkSurfaceKHR - (%p)\n", renderer.surface);

    // Create VkSwapchainKHR handle.
    VC_CHECK(vc_swapchain_create(&renderer));
    printf("Created VkSwapchainKHR - (%p)\n", renderer.swapchain);

    // Create swap chain images.
    vc_swapchain_images_create(&renderer);

    // Create VkRenderPass.
    VC_CHECK(vc_renderpass_create(&renderer));

    // Create descriptor set layout.
    vc_descriptor_setlayout_create(&renderer);

    // Create graphics pipeline.
    vc_graphicspipeline_create(&renderer);

    // Create framebuffers.
    vc_swapchain_framebuffers_create(&renderer);

    // Create command pool.
    VC_CHECK(vc_commandpool_create(&renderer));

    // Create vertex buffer.
    vc_buffer_vertex_create(&renderer, (sizeof(vertices)), vertices);

    // Create index buffer.
    vc_buffer_index_create(&renderer, (sizeof(indices)), indices);

    // Create uniform buffer.
    vc_buffer_uniform_create(&renderer);

    // Create descriptor pool.
    vc_descriptor_pool_create(&renderer);

    // Create descriptor sets.
    vc_descriptorsets_create(&renderer);

    // Create command buffers.
    vc_commandbuffers_create(&renderer, (sizeof(indices) / sizeof(unsigned short)));

    // Create semaphores.
    vc_semaphores_create(&renderer);

    return (renderer);
}

void vc_destroy(vc_renderer_t *pRenderer) {
    vkDeviceWaitIdle(pRenderer->logicalDevice);

    vc_descriptorsets_destroy(pRenderer);
    vc_semaphores_destroy(pRenderer);
    vc_commandbuffers_destroy(pRenderer);
    vc_descriptor_pool_destroy(pRenderer);
    vc_buffer_uniform_destroy(pRenderer);
    vc_buffer_index_destroy(pRenderer);
    vc_buffer_vertex_destroy(pRenderer);
    vc_commandpool_destroy(pRenderer);
    vc_swapchain_framebuffers_destroy(pRenderer);
    vc_graphicspipeline_destroy(pRenderer);
    vc_renderpass_destroy(pRenderer);
    vc_descriptor_setlayout_destroy(pRenderer);

    vc_swapchain_images_destroy(pRenderer);
    vc_swapchain_destroy(pRenderer);
    vc_surface_destroy(pRenderer);
    vc_device_destroy(pRenderer);
    vc_instance_destroy(&pRenderer->instance);
}

unsigned int currentFrame = 0;
void vc_draw(vc_renderer_t *pRenderer) {
    vkWaitForFences(pRenderer->logicalDevice, 1, &pRenderer->pInFlightFences[currentFrame], VK_TRUE, UINT64_MAX);

    VkResult result = vkAcquireNextImageKHR(pRenderer->logicalDevice, pRenderer->swapchain, UINT64_MAX, pRenderer->pImageAvalibleSemaphores[currentFrame], VK_NULL_HANDLE, &pRenderer->imageIndex);
    if (result == VK_ERROR_OUT_OF_DATE_KHR) {
        vc_swapchain_recreate(pRenderer, (sizeof(indices) / sizeof(unsigned short)));
        return;
    } else if (result != VK_SUCCESS && result != VK_SUBOPTIMAL_KHR) {}


    VkSemaphore waitSemaphores[]        = { pRenderer->pImageAvalibleSemaphores[currentFrame] };
    VkPipelineStageFlags waitStages[]   = { VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT };
    VkSemaphore signalSemaphores[]      = { pRenderer->pRenderFinishedSemaphores[currentFrame] };

    VkSubmitInfo submitInfo = {};
    submitInfo.sType                = VK_STRUCTURE_TYPE_SUBMIT_INFO;
    submitInfo.waitSemaphoreCount   = 1;
    submitInfo.pWaitSemaphores      = waitSemaphores;
    submitInfo.pWaitDstStageMask    = waitStages;
    submitInfo.commandBufferCount   = 1;
    submitInfo.pCommandBuffers      = &pRenderer->pCommandBuffers[pRenderer->imageIndex];
    submitInfo.signalSemaphoreCount = 1;
    submitInfo.pSignalSemaphores    = signalSemaphores;

    vkResetFences(pRenderer->logicalDevice, 1, &pRenderer->pInFlightFences[currentFrame]);
    VC_CHECK(vkQueueSubmit(pRenderer->graphicsQueue, 1, &submitInfo, pRenderer->pInFlightFences[currentFrame]));

    VkPresentInfoKHR presentInfo = {};
    presentInfo.sType               = VK_STRUCTURE_TYPE_PRESENT_INFO_KHR;
    presentInfo.waitSemaphoreCount  = 1;
    presentInfo.pWaitSemaphores     = signalSemaphores;
    presentInfo.swapchainCount      = 1;
    presentInfo.pSwapchains         = &pRenderer->swapchain;
    presentInfo.pImageIndices       = &pRenderer->imageIndex;
    presentInfo.pResults            = NULL;

    result = vkQueuePresentKHR(pRenderer->presentQueue, &presentInfo);
    if (result == VK_ERROR_OUT_OF_DATE_KHR || result == VK_SUBOPTIMAL_KHR) {
        vc_swapchain_recreate(pRenderer, (sizeof(indices) / sizeof(unsigned short)));
    } else if (result != VK_SUCCESS) {}

    vkQueueWaitIdle(pRenderer->presentQueue);
    currentFrame = (currentFrame + 1) % VC_MAX_FRAMES_IN_FLIGHT;
}
