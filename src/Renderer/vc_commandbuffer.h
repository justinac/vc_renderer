#ifndef VC_COMMANDBUFFER_H
#define VC_COMMANDBUFFER_H

#include "vc_def.h"
#include "../GUI/window.h"

VkResult vc_commandpool_create(vc_renderer_t *pRenderer);
void vc_commandpool_destroy(vc_renderer_t *pRenderer);

void vc_commandbuffers_create(vc_renderer_t *pRenderer, unsigned int indicesCount);
void vc_commandbuffers_destroy(vc_renderer_t *pRenderer);

#endif // VC_COMMANDBUFFER_H
