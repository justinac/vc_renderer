#ifndef VC_DESCRIPTOR_H
#define VC_DESCRIPTOR_H

#include "vc_def.h"
#include "../Math/math.h"

void vc_descriptor_setlayout_create(vc_renderer_t *pRenderer);
void vc_descriptor_setlayout_destroy(vc_renderer_t *pRenderer);

void vc_descriptor_pool_create(vc_renderer_t *pRenderer);
void vc_descriptor_pool_destroy(vc_renderer_t *pRenderer);

void vc_descriptorsets_create(vc_renderer_t *pRenderer);
void vc_descriptorsets_destroy(vc_renderer_t *pRenderer);

#endif // VC_DESCRIPTOR_H
