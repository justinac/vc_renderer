#ifndef VC_SEMAPHORE_H
#define VC_SEMAPHORE_H

#include "vc_def.h"

void vc_semaphores_create(vc_renderer_t *pRenderer);
void vc_semaphores_destroy(vc_renderer_t *pRenderer);

#endif // VC_SEMAPHORE_H
