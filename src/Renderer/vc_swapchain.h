#ifndef SWAPCHAIN_H
#define SWAPCHAIN_H

#include "vc_def.h"
#include "../GUI/window.h"

VkResult vc_swapchain_create(vc_renderer_t *pRenderer);
void vc_swapchain_destroy(vc_renderer_t *pRenderer);
void vc_swapchain_recreate(vc_renderer_t *pRenderer, unsigned int indicesCount);

void vc_swapchain_images_create(vc_renderer_t *pRenderer);
void vc_swapchain_images_destroy(vc_renderer_t *pRenderer);

void vc_swapchain_framebuffers_create(vc_renderer_t *pRenderer);
void vc_swapchain_framebuffers_destroy(vc_renderer_t *pRenderer);

#endif // SWAPCHAIN_H
