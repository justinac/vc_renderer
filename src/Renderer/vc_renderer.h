#ifndef VC_RENDERER_H
#define VC_RENDERER_H

#include "vc_def.h"
#include "../Math/math.h"
#include "../GUI/window.h"

vc_renderer_t vc_create(const char *pApplicationName, GLFWwindow* window);
void vc_destroy(vc_renderer_t *pRenderer);
void vc_draw(vc_renderer_t *pRenderer);

#endif // VC_RENDERER_H
