#include "vc_graphicspipeline.h"

VkVertexInputBindingDescription vc_get_vertexBindingDescription(unsigned int binding, unsigned int stride, VkVertexInputRate inputRate) {
    VkVertexInputBindingDescription vertexBindingDescription = {};
    vertexBindingDescription.binding    = binding;
    vertexBindingDescription.stride     = stride;
    vertexBindingDescription.inputRate  = inputRate;

    return vertexBindingDescription;
}

VkVertexInputAttributeDescription vc_get_vertexAttributeDescription(unsigned int location, unsigned int binding, VkFormat format, unsigned int offset) {
    VkVertexInputAttributeDescription description = {};
    description.location   = location;
    description.binding    = binding;
    description.format     = format;
    description.offset     = offset;

    return description;
}

void vc_graphicspipeline_create(vc_renderer_t *pRenderer) {
    VkShaderModule vertShaderModule = vc_graphicspipeline_shadermodule_create(pRenderer, "../build/shaders/vert.spv");
    VkShaderModule fragShaderModule = vc_graphicspipeline_shadermodule_create(pRenderer, "../build/shaders/frag.spv");

    VkPipelineShaderStageCreateInfo vertShaderStageInfo = {};
    vertShaderStageInfo.sType   = VK_STRUCTURE_TYPE_PIPELINE_SHADER_STAGE_CREATE_INFO;
    vertShaderStageInfo.stage   = VK_SHADER_STAGE_VERTEX_BIT;
    vertShaderStageInfo.module  = vertShaderModule;
    vertShaderStageInfo.pName   = "main";

    VkPipelineShaderStageCreateInfo fragShaderStageInfo = {};
    fragShaderStageInfo.sType   = VK_STRUCTURE_TYPE_PIPELINE_SHADER_STAGE_CREATE_INFO;
    fragShaderStageInfo.stage   = VK_SHADER_STAGE_FRAGMENT_BIT;
    fragShaderStageInfo.module  = fragShaderModule;
    fragShaderStageInfo.pName   = "main";

    VkPipelineShaderStageCreateInfo shaderStages[] = { vertShaderStageInfo, fragShaderStageInfo };

    VkPipelineVertexInputStateCreateInfo vertexInputInfo = {};
    vertexInputInfo.sType = VK_STRUCTURE_TYPE_PIPELINE_VERTEX_INPUT_STATE_CREATE_INFO;

    VkVertexInputBindingDescription bindingDescription  = vc_get_vertexBindingDescription(0, sizeof(vertex_t), VK_VERTEX_INPUT_RATE_VERTEX);
    VkVertexInputAttributeDescription vertexDescription = vc_get_vertexAttributeDescription(0, 0, VK_FORMAT_R32G32B32_SFLOAT, offsetof(vertex_t, position));
    VkVertexInputAttributeDescription colorDescription  = vc_get_vertexAttributeDescription(1, 0, VK_FORMAT_R32G32B32_SFLOAT, offsetof(vertex_t, color));

    VkVertexInputAttributeDescription attributeDescriptions[2] = { vertexDescription, colorDescription };

    vertexInputInfo.vertexBindingDescriptionCount   = 1;
    vertexInputInfo.pVertexBindingDescriptions      = &bindingDescription;
    vertexInputInfo.vertexAttributeDescriptionCount = 2;
    vertexInputInfo.pVertexAttributeDescriptions    = attributeDescriptions;

    VkPipelineInputAssemblyStateCreateInfo inputAssembly = {};
    inputAssembly.sType                     = VK_STRUCTURE_TYPE_PIPELINE_INPUT_ASSEMBLY_STATE_CREATE_INFO;
    inputAssembly.topology                  = VK_PRIMITIVE_TOPOLOGY_TRIANGLE_LIST;
    inputAssembly.primitiveRestartEnable    = VK_FALSE;

    VkViewport viewport = {};
    viewport.x = 0;
    viewport.y = 0;

    int w, h;
    glfwGetWindowSize(pRenderer->pWindow, &w, &h);

    viewport.width      = w;
    viewport.height     = h;

    viewport.minDepth = 0;
    viewport.maxDepth = 1;

    VkRect2D scissor = {};
    scissor.offset = (VkOffset2D){ 0, 0 };
    scissor.extent = (VkExtent2D){ w, h };

    VkPipelineViewportStateCreateInfo viewportState = {};
    viewportState.sType         = VK_STRUCTURE_TYPE_PIPELINE_VIEWPORT_STATE_CREATE_INFO;
    viewportState.viewportCount = 1;
    viewportState.pViewports    = &viewport;
    viewportState.scissorCount  = 1;
    viewportState.pScissors     = &scissor;

    VkPipelineRasterizationStateCreateInfo rasterizer = {};
    rasterizer.sType                    = VK_STRUCTURE_TYPE_PIPELINE_RASTERIZATION_STATE_CREATE_INFO;
    rasterizer.depthClampEnable         = VK_FALSE;
    rasterizer.rasterizerDiscardEnable  = VK_FALSE;
    rasterizer.polygonMode              = VK_POLYGON_MODE_FILL;
    rasterizer.cullMode                 = VK_CULL_MODE_BACK_BIT;
    rasterizer.frontFace                = VK_FRONT_FACE_CLOCKWISE;
    rasterizer.depthBiasEnable          = VK_FALSE;
    rasterizer.depthBiasConstantFactor  = 0;
    rasterizer.depthBiasClamp           = 0;
    rasterizer.depthBiasSlopeFactor     = 0;
    rasterizer.lineWidth                = 1;

    VkPipelineMultisampleStateCreateInfo multisampling = {};
    multisampling.sType                 = VK_STRUCTURE_TYPE_PIPELINE_MULTISAMPLE_STATE_CREATE_INFO;
    multisampling.rasterizationSamples  = VK_SAMPLE_COUNT_1_BIT;
    multisampling.sampleShadingEnable   = VK_FALSE;
    multisampling.minSampleShading      = 1;
    multisampling.pSampleMask           = NULL;
    multisampling.alphaToCoverageEnable = VK_FALSE;
    multisampling.alphaToOneEnable      = VK_FALSE;

    VkPipelineColorBlendAttachmentState colorBlendAttachment = {};
    colorBlendAttachment.blendEnable            = VK_FALSE;
    colorBlendAttachment.srcColorBlendFactor    = VK_BLEND_FACTOR_ONE;  // VK_BLEND_FACTOR_SRC_ALPHA
    colorBlendAttachment.dstColorBlendFactor    = VK_BLEND_FACTOR_ZERO; // VK_BLEND_FACTOR_ONE_MINUS_SRC_ALPHA
    colorBlendAttachment.colorBlendOp           = VK_BLEND_OP_ADD;
    colorBlendAttachment.srcAlphaBlendFactor    = VK_BLEND_FACTOR_ONE;
    colorBlendAttachment.dstAlphaBlendFactor    = VK_BLEND_FACTOR_ZERO;
    colorBlendAttachment.alphaBlendOp           = VK_BLEND_OP_ADD;
    colorBlendAttachment.colorWriteMask         = VK_COLOR_COMPONENT_R_BIT | VK_COLOR_COMPONENT_G_BIT | VK_COLOR_COMPONENT_B_BIT | VK_COLOR_COMPONENT_A_BIT;

    VkPipelineColorBlendStateCreateInfo colorBlending = {};
    colorBlending.sType             = VK_STRUCTURE_TYPE_PIPELINE_COLOR_BLEND_STATE_CREATE_INFO;
    colorBlending.logicOpEnable     = VK_FALSE;
    colorBlending.logicOp           = VK_LOGIC_OP_COPY;
    colorBlending.attachmentCount   = 1;
    colorBlending.pAttachments      = &colorBlendAttachment;
    colorBlending.blendConstants[0] = 0;
    colorBlending.blendConstants[1] = 0;
    colorBlending.blendConstants[2] = 0;
    colorBlending.blendConstants[3] = 0;

    VkPipelineLayoutCreateInfo pipelineLayoutInfo = {};
    pipelineLayoutInfo.sType                    = VK_STRUCTURE_TYPE_PIPELINE_LAYOUT_CREATE_INFO;
    pipelineLayoutInfo.setLayoutCount           = 1;
    pipelineLayoutInfo.pSetLayouts              = &pRenderer->descriptorSetLayout;
    pipelineLayoutInfo.pushConstantRangeCount   = 0;
    pipelineLayoutInfo.pPushConstantRanges      = NULL;

    VC_CHECK(vkCreatePipelineLayout(pRenderer->logicalDevice, &pipelineLayoutInfo, NULL, &pRenderer->pipelineLayout));

    VkGraphicsPipelineCreateInfo pipelineInfo = {};
    pipelineInfo.sType                  = VK_STRUCTURE_TYPE_GRAPHICS_PIPELINE_CREATE_INFO;
    pipelineInfo.stageCount             = 2;
    pipelineInfo.pStages                = shaderStages;
    pipelineInfo.pVertexInputState      = &vertexInputInfo;
    pipelineInfo.pInputAssemblyState    = &inputAssembly;
    pipelineInfo.pViewportState         = &viewportState;
    pipelineInfo.pRasterizationState    = &rasterizer;
    pipelineInfo.pMultisampleState      = &multisampling;
    pipelineInfo.pDepthStencilState     = NULL;
    pipelineInfo.pColorBlendState       = &colorBlending;
    pipelineInfo.pDynamicState          = NULL;
    pipelineInfo.layout                 = pRenderer->pipelineLayout;
    pipelineInfo.renderPass             = pRenderer->renderpass;
    pipelineInfo.subpass                = 0;
    pipelineInfo.basePipelineHandle     = VK_NULL_HANDLE;
    pipelineInfo.basePipelineIndex      = -1;

    VC_CHECK(vkCreateGraphicsPipelines(pRenderer->logicalDevice, VK_NULL_HANDLE, 1, &pipelineInfo, NULL, &pRenderer->graphicsPipeline));

    vkDestroyShaderModule(pRenderer->logicalDevice, fragShaderModule, VK_NULL_HANDLE);
    vkDestroyShaderModule(pRenderer->logicalDevice, vertShaderModule, VK_NULL_HANDLE);
}

void vc_graphicspipeline_destroy(vc_renderer_t *pRenderer) {
    vkDestroyPipeline(pRenderer->logicalDevice, pRenderer->graphicsPipeline, VK_NULL_HANDLE);
    vkDestroyPipelineLayout(pRenderer->logicalDevice, pRenderer->pipelineLayout, VK_NULL_HANDLE);
}

VkShaderModule vc_graphicspipeline_shadermodule_create(vc_renderer_t *pRenderer, const char *filePath) {
    FILE *fp = fopen(filePath, "r+b");

    if (!fp) {
        exit(EXIT_FAILURE);
    }

    fseek(fp, 0, SEEK_END);
    unsigned int len = ftell(fp);
    rewind(fp);
    char *data = calloc(len + 1, sizeof(char));
    fread(data, 1, len, fp);

    fclose(fp);

    VkShaderModuleCreateInfo shaderModuleInfo = {};
    shaderModuleInfo.sType    = VK_STRUCTURE_TYPE_SHADER_MODULE_CREATE_INFO;
    shaderModuleInfo.codeSize = len;
    shaderModuleInfo.pCode    = (const unsigned int *)data;

    VkShaderModule shaderModule;

    VC_CHECK(vkCreateShaderModule(pRenderer->logicalDevice, &shaderModuleInfo, NULL, &shaderModule));

    return shaderModule;
}
