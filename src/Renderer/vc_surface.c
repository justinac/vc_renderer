#include "vc_surface.h"

VkResult vc_surface_create(vc_renderer_t *pRenderer) {
    return (glfwCreateWindowSurface(pRenderer->instance, pRenderer->pWindow, NULL, &pRenderer->surface));
}

void vc_surface_destroy(vc_renderer_t *pRenderer) {
    vkDestroySurfaceKHR(pRenderer->instance, pRenderer->surface, VK_NULL_HANDLE);
}