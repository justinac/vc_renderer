#ifndef VC_BUFFER_H
#define VC_BUFFER_H

#include "vc_def.h"
#include "vc_gpu.h"

#include "../Math/math.h"
#include "../GUI/window.h"

void vc_buffer_copy(vc_renderer_t *pRenderer, VkBuffer dest, VkBuffer src, VkDeviceSize size);
void vc_buffer_create(vc_renderer_t *pRenderer, VkDeviceSize size, VkBufferUsageFlags usage, VkMemoryPropertyFlags properties, VkBuffer *pBuffer, VkDeviceMemory *pBufferMemory);

void vc_buffer_vertex_create(vc_renderer_t *pRenderer, unsigned int verticesSize, vertex_t *vertices);
void vc_buffer_vertex_destroy(vc_renderer_t *pRenderer);

void vc_buffer_index_create(vc_renderer_t *pRenderer, unsigned int indicesSize, unsigned short *indices);
void vc_buffer_index_destroy(vc_renderer_t *pRenderer);

void vc_buffer_uniform_create(vc_renderer_t *pRenderer);
void vc_buffer_uniform_destroy(vc_renderer_t *pRenderer);
void vc_buffer_uniform_update(vc_renderer_t *pRenderer, unsigned int imageIndex);

#endif // VC_BUFFER_H
