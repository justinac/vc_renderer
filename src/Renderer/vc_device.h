#ifndef VC_DEVICE_H
#define VC_DEVICE_H

#include "vc_def.h"

VkResult vc_device_create(vc_renderer_t *pRenderer);
void vc_device_destroy(vc_renderer_t *pRenderer);

#endif // VC_DEVICE_H
