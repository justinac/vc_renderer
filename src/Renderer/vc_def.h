#ifndef VC_DEF_H
#define VC_DEF_H

#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <stddef.h>

#include <string.h>
#include <assert.h>
#include <errno.h>

#include <vulkan/vulkan.h>
#include <GLFW/glfw3.h>
#include <stb_image.h>

#define VC_CHECK(call) assert(call == VK_SUCCESS)
#define VC_PRINTLN(text, ptr) printf("%s:\t%p\n", text, ptr)

#define VC_SINGLE_BUFFERED 1
#define VC_DOUBLE_BUFFERED 2
#define VC_TRIPLE_BUFFERED 3

#define VC_MAX_FRAMES_IN_FLIGHT 2
#define VC_DEFAULT_IMAGE_FORMAT (VK_FORMAT_B8G8R8A8_UNORM)

typedef struct {
    VkPhysicalDevice            physicalDevice;
    VkPhysicalDeviceFeatures    features;
    VkPhysicalDeviceProperties  properties;
} vc_gpu_t;

typedef struct {
    const char             *pApplicationName;
    GLFWwindow             *pWindow;
    VkInstance              instance;
    vc_gpu_t                gpu;
    VkDevice                logicalDevice;
    unsigned int            queueFamilyIndex;
    VkQueue                 graphicsQueue;
    VkQueue                 presentQueue;
    VkSurfaceKHR            surface;
    VkSwapchainKHR          swapchain;
    VkFormat                format;
    VkExtent2D              size;
    unsigned int            imageCount;
    VkImage                *pImages;
    VkImageView            *pImageViews;
    VkFramebuffer          *pFrameBuffers;
    VkRenderPass            renderpass;
    VkPipelineLayout        pipelineLayout;
    VkPipeline              graphicsPipeline;
    VkCommandPool           commandPool;
    VkCommandBuffer        *pCommandBuffers;
    VkSemaphore            *pImageAvalibleSemaphores;
    VkSemaphore            *pRenderFinishedSemaphores;
    VkFence                *pInFlightFences;
    VkBuffer                vertexBuffer;
    VkDeviceMemory          vertexBufferMemory;
    VkBuffer                indexBuffer;
    VkDeviceMemory          indexBufferMemory;
    VkBuffer               *pUniformBuffers;
    VkDeviceMemory         *pUniformBuffersMemory;
    unsigned int            imageIndex;
    VkDescriptorSetLayout   descriptorSetLayout;
    VkDescriptorPool        descriptorPool;
    VkDescriptorSet        *pDescriptorSets;
} vc_renderer_t;

#endif // VC_DEF_H