#include "vc_commandbuffer.h"

VkResult vc_commandpool_create(vc_renderer_t *pRenderer) {
    VkCommandPoolCreateInfo commandPoolInfo = {};
    commandPoolInfo.sType             = VK_STRUCTURE_TYPE_COMMAND_POOL_CREATE_INFO;
    commandPoolInfo.queueFamilyIndex  = pRenderer->queueFamilyIndex;

    return (vkCreateCommandPool(pRenderer->logicalDevice, &commandPoolInfo, NULL, &pRenderer->commandPool));
}

void vc_commandpool_destroy(vc_renderer_t *pRenderer) {
    vkDestroyCommandPool(pRenderer->logicalDevice, pRenderer->commandPool, VK_NULL_HANDLE);
}

void vc_commandbuffers_create(vc_renderer_t *pRenderer, unsigned int indicesCount) {
    pRenderer->pCommandBuffers = (VkCommandBuffer*)malloc(sizeof(VkCommandBuffer) * pRenderer->imageCount);

    VkCommandBufferAllocateInfo allocInfo = {};
    allocInfo.sType                 = VK_STRUCTURE_TYPE_COMMAND_BUFFER_ALLOCATE_INFO;
    allocInfo.commandPool           = pRenderer->commandPool;
    allocInfo.level                 = VK_COMMAND_BUFFER_LEVEL_PRIMARY;
    allocInfo.commandBufferCount    = pRenderer->imageCount;

    VC_CHECK(vkAllocateCommandBuffers(pRenderer->logicalDevice, &allocInfo, pRenderer->pCommandBuffers));

    for (int i = 0; i < allocInfo.commandBufferCount; i++) {
        VkCommandBufferBeginInfo beginInfo = {};
        beginInfo.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_BEGIN_INFO;
        beginInfo.flags = VK_COMMAND_BUFFER_USAGE_SIMULTANEOUS_USE_BIT;

        VC_CHECK(vkBeginCommandBuffer(pRenderer->pCommandBuffers[i], &beginInfo));

        VkRenderPassBeginInfo renderPassInfo = {};
        renderPassInfo.sType                = VK_STRUCTURE_TYPE_RENDER_PASS_BEGIN_INFO;
        renderPassInfo.renderPass           = pRenderer->renderpass;
        renderPassInfo.framebuffer          = pRenderer->pFrameBuffers[i];
        renderPassInfo.renderArea.offset    = (VkOffset2D){ 0, 0 };
        renderPassInfo.renderArea.extent    = window_get_size(pRenderer->pWindow);

        VkClearValue clearColor         = { 0.01f, 0.01f, 0.01f, 1.0f };
        renderPassInfo.clearValueCount  = 1;
        renderPassInfo.pClearValues     = &clearColor;

        vkCmdBeginRenderPass(pRenderer->pCommandBuffers[i], &renderPassInfo, VK_SUBPASS_CONTENTS_INLINE);
        vkCmdBindPipeline(pRenderer->pCommandBuffers[i], VK_PIPELINE_BIND_POINT_GRAPHICS, pRenderer->graphicsPipeline);

        VkDeviceSize offsets[] = { 0 };
        vkCmdBindVertexBuffers(pRenderer->pCommandBuffers[i], 0, 1, &pRenderer->vertexBuffer, offsets);
        vkCmdBindIndexBuffer(pRenderer->pCommandBuffers[i], pRenderer->indexBuffer, 0, VK_INDEX_TYPE_UINT16);

        vkCmdBindDescriptorSets(pRenderer->pCommandBuffers[i], VK_PIPELINE_BIND_POINT_GRAPHICS, pRenderer->pipelineLayout, 0, 1, &pRenderer->pDescriptorSets[i], 0, NULL);
        vkCmdDrawIndexed(pRenderer->pCommandBuffers[i], indicesCount, 1, 0, 0, 0);

        vkCmdEndRenderPass(pRenderer->pCommandBuffers[i]);

        VC_CHECK(vkEndCommandBuffer(pRenderer->pCommandBuffers[i]));
    }
}

void vc_commandbuffers_destroy(vc_renderer_t *pRenderer) {
    vkFreeCommandBuffers(pRenderer->logicalDevice, pRenderer->commandPool, pRenderer->imageCount, pRenderer->pCommandBuffers);

    free(pRenderer->pCommandBuffers);
}
