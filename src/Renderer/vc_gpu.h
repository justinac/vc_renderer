// Author: @justinac.
// Description: .
// License: MIT.

#ifndef VC_GPU_H
#define VC_GPU_H

#include "vc_def.h"

// Select a GPU to use.
void vc_gpu_list(vc_renderer_t *pRenderer);

// See if device supports specific memory type.
unsigned int gpu_get_memory_type(vc_renderer_t *pRenderer, unsigned int typeFilter, VkMemoryPropertyFlags properties);

#endif // VC_GPU_H
