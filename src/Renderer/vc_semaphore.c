#include "vc_semaphore.h"

void vc_semaphores_create(vc_renderer_t *pRenderer) {
    pRenderer->pImageAvalibleSemaphores   = malloc(sizeof(VkSemaphore)    * VC_MAX_FRAMES_IN_FLIGHT);
    pRenderer->pRenderFinishedSemaphores  = malloc(sizeof(VkSemaphore)    * VC_MAX_FRAMES_IN_FLIGHT);
    pRenderer->pInFlightFences            = malloc(sizeof(VkFence)        * VC_MAX_FRAMES_IN_FLIGHT);

    VkSemaphoreCreateInfo semaphoreInfo = {};
    semaphoreInfo.sType = VK_STRUCTURE_TYPE_SEMAPHORE_CREATE_INFO;

    VkFenceCreateInfo fenceInfo = {};
    fenceInfo.sType = VK_STRUCTURE_TYPE_FENCE_CREATE_INFO;
    fenceInfo.flags = VK_FENCE_CREATE_SIGNALED_BIT;

    for (int i = 0; i < VC_MAX_FRAMES_IN_FLIGHT; i++) {
        VC_CHECK(vkCreateSemaphore(pRenderer->logicalDevice, &semaphoreInfo, NULL, &pRenderer->pImageAvalibleSemaphores[i]));
        VC_CHECK(vkCreateSemaphore(pRenderer->logicalDevice, &semaphoreInfo, NULL, &pRenderer->pRenderFinishedSemaphores[i]));
        VC_CHECK(vkCreateFence(pRenderer->logicalDevice, &fenceInfo, NULL, &pRenderer->pInFlightFences[i]));
    }
}

void vc_semaphores_destroy(vc_renderer_t *pRenderer) {
    for (int i = 0; i < VC_MAX_FRAMES_IN_FLIGHT; i++) {
        vkDestroySemaphore(pRenderer->logicalDevice, pRenderer->pRenderFinishedSemaphores[i], VK_NULL_HANDLE);
        vkDestroySemaphore(pRenderer->logicalDevice, pRenderer->pImageAvalibleSemaphores[i], VK_NULL_HANDLE);
        vkDestroyFence(pRenderer->logicalDevice, pRenderer->pInFlightFences[i], VK_NULL_HANDLE);
    }

    free(pRenderer->pImageAvalibleSemaphores);
    free(pRenderer->pRenderFinishedSemaphores);
    free(pRenderer->pInFlightFences);
}
