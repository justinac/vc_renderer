#ifndef VC_RENDERPASS_H
#define VC_RENDERPASS_H

#include "vc_def.h"

VkResult vc_renderpass_create(vc_renderer_t *pRenderer);
void vc_renderpass_destroy(vc_renderer_t *pRenderer);

#endif // VC_RENDERPASS_H
