#include "vc_descriptor.h"

void vc_descriptor_setlayout_create(vc_renderer_t *pRenderer) {
    VkDescriptorSetLayoutBinding uboLayoutBinding = {};
    uboLayoutBinding.binding            = 0;
    uboLayoutBinding.descriptorType     = VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER;
    uboLayoutBinding.descriptorCount    = 1;
    uboLayoutBinding.stageFlags         = VK_SHADER_STAGE_VERTEX_BIT;
    uboLayoutBinding.pImmutableSamplers = NULL;

    VkDescriptorSetLayoutCreateInfo layoutInfo = {};
    layoutInfo.sType        = VK_STRUCTURE_TYPE_DESCRIPTOR_SET_LAYOUT_CREATE_INFO;
    layoutInfo.bindingCount = 1;
    layoutInfo.pBindings    = &uboLayoutBinding;

    VC_CHECK(vkCreateDescriptorSetLayout(pRenderer->logicalDevice, &layoutInfo, NULL, &pRenderer->descriptorSetLayout));
}

void vc_descriptor_setlayout_destroy(vc_renderer_t *pRenderer) {
    vkDestroyDescriptorSetLayout(pRenderer->logicalDevice, pRenderer->descriptorSetLayout, VK_NULL_HANDLE);
}

void vc_descriptor_pool_create(vc_renderer_t *pRenderer) {
    VkDescriptorPoolSize poolSize = {};
    poolSize.type               = VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER;
    poolSize.descriptorCount    = pRenderer->imageCount;

    VkDescriptorPoolCreateInfo poolInfo = {};
    poolInfo.sType          = VK_STRUCTURE_TYPE_DESCRIPTOR_POOL_CREATE_INFO;
    poolInfo.maxSets        = pRenderer->imageCount;
    poolInfo.poolSizeCount  = 1;
    poolInfo.pPoolSizes     = &poolSize;

    VC_CHECK(vkCreateDescriptorPool(pRenderer->logicalDevice, &poolInfo, NULL, &pRenderer->descriptorPool));
}

void vc_descriptor_pool_destroy(vc_renderer_t *pRenderer) {
    vkDestroyDescriptorPool(pRenderer->logicalDevice, pRenderer->descriptorPool, VK_NULL_HANDLE);
}

void vc_descriptorsets_create(vc_renderer_t *pRenderer) {
    pRenderer->pDescriptorSets = malloc(sizeof(VkDescriptorSet) * pRenderer->imageCount);

    VkDescriptorSetLayout layouts[] = { pRenderer->descriptorSetLayout, pRenderer->descriptorSetLayout };

    VkDescriptorSetAllocateInfo allocInfo = {};
    allocInfo.sType                 = VK_STRUCTURE_TYPE_DESCRIPTOR_SET_ALLOCATE_INFO;
    allocInfo.descriptorPool        = pRenderer->descriptorPool;
    allocInfo.descriptorSetCount    = pRenderer->imageCount;
    allocInfo.pSetLayouts           = layouts;

    VC_CHECK(vkAllocateDescriptorSets(pRenderer->logicalDevice, &allocInfo, pRenderer->pDescriptorSets));

    for (int i = 0; i < pRenderer->imageCount; i++) {
        VkDescriptorBufferInfo bufferInfo = {};
        bufferInfo.buffer   = pRenderer->pUniformBuffers[i];
        bufferInfo.offset   = 0;
        bufferInfo.range    = sizeof(ubo_t);

        VkWriteDescriptorSet descriptorWrite = {};
        descriptorWrite.sType               = VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET;
        descriptorWrite.dstSet              = pRenderer->pDescriptorSets[i];
        descriptorWrite.dstBinding          = 0;
        descriptorWrite.dstArrayElement     = 0;
        descriptorWrite.descriptorCount     = 1;
        descriptorWrite.descriptorType      = VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER;
        descriptorWrite.pImageInfo          = NULL;
        descriptorWrite.pBufferInfo         = &bufferInfo;
        descriptorWrite.pTexelBufferView    = NULL;

        vkUpdateDescriptorSets(pRenderer->logicalDevice, 1, &descriptorWrite, 0, NULL);
    }
}

void vc_descriptorsets_destroy(vc_renderer_t *pRenderer) {
    free(pRenderer->pDescriptorSets);
}