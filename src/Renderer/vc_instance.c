#include "vc_instance.h"

VkResult vc_instance_create(VkInstance *pInstance, const char *pApplicationName) {
    // Using Vulkan API version 1.1.0.
    // Application info is required to pass into instance create info.
    VkApplicationInfo applicationInfo = {};
    applicationInfo.sType               = VK_STRUCTURE_TYPE_APPLICATION_INFO;
    applicationInfo.pNext               = NULL;
    applicationInfo.pApplicationName    = pApplicationName;
    applicationInfo.applicationVersion  = VK_MAKE_VERSION(1, 1, 0);
    applicationInfo.pEngineName         = pApplicationName;
    applicationInfo.engineVersion       = VK_MAKE_VERSION(1, 1, 0);
    applicationInfo.apiVersion          = VK_API_VERSION_1_1;

    // Instance create info.
    VkInstanceCreateInfo instanceInfo = {};
    instanceInfo.sType                    = VK_STRUCTURE_TYPE_INSTANCE_CREATE_INFO;
    instanceInfo.pApplicationInfo         = &applicationInfo;
    instanceInfo.enabledLayerCount        = 1;

    // Validation layers are used for debugging where
    // in the vulkan context creation / deletion errors occur.
    #if 1 // Set to 0 to disable validation layers.
        unsigned int glfwExtensionCount = 0;
        const char **glfwExtensions = glfwGetRequiredInstanceExtensions(&glfwExtensionCount);

        {
            unsigned int amountOfLayers = 0;
            vkEnumerateInstanceLayerProperties(&amountOfLayers, NULL);
            VkLayerProperties *layers = (VkLayerProperties*)malloc(sizeof(VkLayerProperties) * amountOfLayers);
            vkEnumerateInstanceLayerProperties(&amountOfLayers, layers);

            free(layers);
        }

        const char *validationLayers[] = {
            "VK_LAYER_LUNARG_standard_validation"
        };

        instanceInfo.ppEnabledLayerNames      = validationLayers;
        instanceInfo.enabledExtensionCount    = glfwExtensionCount;
        instanceInfo.ppEnabledExtensionNames  = glfwExtensions;
    #else
        instanceInfo.ppEnabledLayerNames      = NULL;
        instanceInfo.enabledExtensionCount    = 0;
        instanceInfo.ppEnabledExtensionNames  = NULL;
    #endif // Validation layers.

    return (vkCreateInstance(&instanceInfo, NULL, pInstance));
}

void vc_instance_destroy(VkInstance *pInstance) {
    vkDestroyInstance(*pInstance, VK_NULL_HANDLE);
}
