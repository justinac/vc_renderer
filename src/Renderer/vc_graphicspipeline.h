#ifndef VC_GRAPHICSPIPELINE_H
#define VC_GRAPHICSPIPELINE_H

#include "vc_def.h"
#include "../Math/math.h"

void vc_graphicspipeline_create(vc_renderer_t *pRenderer);
void vc_graphicspipeline_destroy(vc_renderer_t *pRenderer);

VkShaderModule vc_graphicspipeline_shadermodule_create(vc_renderer_t *pRenderer, const char *filePath);

#endif // VC_GRAPHICSPIPELINE_H
