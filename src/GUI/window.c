#include "window.h"

window_t window_create(unsigned int width, unsigned int height, const char *title) {
    if (!glfwInit()) {}

    glfwWindowHint(GLFW_CLIENT_API, GLFW_NO_API);
    glfwWindowHint(GLFW_RESIZABLE, GLFW_FALSE);

    window_t window;
    window.pHandle  = glfwCreateWindow(width, height, title, 0, 0);
    window.width    = width;
    window.height   = height;
    window.title    = title;

    if (!window.pHandle) {}
    
    glfwSetInputMode(window.pHandle, GLFW_CURSOR, GLFW_CURSOR_HIDDEN);

    return window;
}

void window_destroy(GLFWwindow *pWindow) {
    glfwDestroyWindow(pWindow);
    glfwTerminate();
}

VkExtent2D window_get_size(GLFWwindow *pWindow) {
    VkExtent2D size;
    glfwGetWindowSize(pWindow, &size.width, &size.height);

    return size;
}