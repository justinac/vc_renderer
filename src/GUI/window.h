#ifndef WINDOW_H
#define WINDOW_H

#include <GLFW/glfw3.h>

#include "../Renderer/vc_def.h"

typedef struct {
    GLFWwindow  *pHandle;
    unsigned int width;
    unsigned int height;
    const char  *title;
} window_t;

window_t window_create(unsigned int width, unsigned int height, const char *title);
void window_destroy(GLFWwindow *pWindow);
VkExtent2D window_get_size(GLFWwindow *pWindow);

#endif // WINDOW_H
