#define APPLICATION_NAME "VULKAN_RENDERER"

#include "Renderer/vc_renderer.h"
#include "Renderer/vc_buffer.h"

int main(int argc, char const *argv[]) {
    window_t        window      = window_create(1280, 720, APPLICATION_NAME);
    vc_renderer_t   renderer    = vc_create(APPLICATION_NAME, window.pHandle);

    double lastTime = glfwGetTime();
    double timePerTick = 1.0f / 30.0f;

    while (!glfwWindowShouldClose(window.pHandle)) {
        glfwPollEvents();

        if (glfwGetTime() >= lastTime + timePerTick) {
            lastTime = glfwGetTime();
            vc_draw(&renderer);
            vc_buffer_uniform_update(&renderer, renderer.imageIndex);
        }
    }

    vc_destroy(&renderer);

    glfwDestroyWindow(window.pHandle);
    glfwTerminate();

    return 0;
}
